#include "Variable.h"

const long VariableTable::FALSE = 0;
const long VariableTable::TRUE = 1;

Variable::Variable(const Variable& other, bool minus):
    const_qualifier(other.const_qualifier),
    type_id(other.type_id),
    data(other.data->copy())
{
    /*if (minus){
        if (type_id == 0 || type_id == 1){
        } else {
            throw std::runtime_error("can't apply '-' to this type of const");
        }
    }*/
}

Variable::Variable(TypeID type_id, Data* new_data, bool const_qualifier, bool minus):
    const_qualifier(const_qualifier),
    type_id(type_id),
    data(new_data)
{
    /*if (minus){
        if (type_id == 0 || type_id == 1){
        } else {
            throw std::runtime_error("can't apply '-' to this type of const");
        }
    }*/
}

Operand Variable::get_ptr_operand() const {
    return Operand(data->get_ptr_copy(), Operand::f_address);
}

bool Variable::is_const() const{
    return const_qualifier;
}

TypeID Variable::get_type() const {
    return type_id;
}

VariableTable::VariableTable():
    table {
            Variable(TypeTable::BOOL, false, true),
            Variable(TypeTable::BOOL, true, true),
    }
{}

VariableID VariableTable::add_constant(TypeID type_id, Data* data, bool minus) {
    table.emplace_back(type_id, data, true, minus);
    return table.size()-1;
}

VariableID VariableTable::add_constant(VariableID id, bool minus) {
    table.emplace_back(table.at(id), minus);
    return table.size()-1;
}

VariableID VariableTable::add_variable(const Type& type, TypeID type_id) {
    switch (type.get_nature()){
        case Type::t_int:
            table.emplace_back(type_id, long()); //magic
            return table.size()-1;
        case Type::t_double:
            table.emplace_back(type_id, double());
            return table.size()-1;
        case Type::t_bool:
            table.emplace_back(type_id, bool());
            return table.size()-1;
        case Type::t_char:
            using uchar = unsigned char;
            table.emplace_back(type_id, uchar()); //why doesn't "unsigned char()" work?
            return table.size()-1;
        case Type::t_string:
            table.emplace_back(type_id, std::string());
            return table.size()-1;
        default:
            throw std::logic_error("not implemented");
    }
}

const Variable& VariableTable::at(long a) const{
    return table.at(a);
}

VariableNames::VariableNames():
    std::unordered_map<std::string, VariableID> {
            {"false", VariableTable::FALSE},
            {"true", VariableTable::TRUE},
    }
{}
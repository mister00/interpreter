#include <iostream>
#include <fstream>
#include "Interpreter.h"

int main(int argc, char** argv) {
    if (argc < 2){
        std::cerr << "Specify the name of the file";
        return -1;
    }
    std::ifstream stream(argv[1]);
    Interpreter interpreter(stream);
    if (!stream.is_open()){
        std::cerr << "не удалось открыть файл";
        return -1;
    }
    try{
        interpreter.interpret();
    } catch (std::runtime_error& err){
        std::cerr << err.what() <<std::endl;
        return -1;
    }
    return 0;
}

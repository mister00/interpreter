#pragma once
#include <vector>
#include <unordered_map>
#include "Lexeme.h"
#include "Command.h"

class Type {
public:
    enum TypeNature{
        t_int,
        t_double,
        t_bool,
        t_string,
        t_char,
        t_array,
        t_pointer,
    };
private:
    TypeNature nature;
    long type_reference;
public:
    Type() = default;
    Type(TypeNature nature): nature(nature){}
    Type(TypeNature nature, long type_reference): nature(nature), type_reference(type_reference){}
    TypeNature get_nature() const;
};

using TypeID = long;

class TypeTable{
public:
    static const TypeID INT;
    static const TypeID DOUBLE;
    static const TypeID BOOL;
    static const TypeID CHAR;
    static const TypeID STRING;
private:
    std::vector<Type> table;
    using Operator = std::tuple<TypeID, TypeID, OperatorType>;
    class Hash_tuple{
    public:
        std::size_t operator() (const Operator& action) const{
            std::hash<TypeID> hash1;
            std::hash<OperatorType> hash2;
            return hash1(std::get<0>(action)) ^ hash1(std::get<1>(action)) ^ hash2(std::get<2>(action));
        }
    };
    std::unordered_map<Operator, TypeID, Hash_tuple> operator_table;
    std::unordered_map<Operator, std::shared_ptr<Command>, Hash_tuple> command_table;
    void add_operator(TypeID, TypeID, OperatorType, TypeID);
    void add_command(TypeID, TypeID, OperatorType, Command*);
public:
    TypeID get_result_type(TypeID, TypeID, OperatorType);
    TypeID get_result_type(TypeID, OperatorType);
    std::shared_ptr<Command>& get_command(TypeID, TypeID, OperatorType);
    std::shared_ptr<Command>& get_command(TypeID, OperatorType);
    static TypeID get_type(const Lexeme& lexeme);
    static bool is_logical(TypeID type_id);
    TypeTable();
    const Type& at (long a) const;
};

class TypeNames: public std::unordered_map<std::string, TypeID>{
public:
    TypeNames();
};
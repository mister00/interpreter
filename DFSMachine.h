#pragma once
#include <string>
#include <unordered_map>
#include <unordered_set>
#include "Lexeme.h"

class DFSMachine{
public:
    using State = std::string;
    using Action = std::pair<State, unsigned char>;
private:
    class Hash_pair{
    public:
        std::size_t operator() (const Action& action) const{
            std::hash<State> hash1;
            std::hash<unsigned char> hash2;
            return hash1(std::get<0>(action)) ^ hash2(std::get<1>(action));
        }
    };
public:
    using State_set = std::unordered_set<State>;
    using Instructions = std::unordered_map<Action, State, Hash_pair>;
    enum Verdict{v_next, v_done, v_error};
    using Outcome = std::pair<Verdict, Lexeme::LexemeString>;
private:
    Instructions instructions;
    State state = "start";
    State_set terminal_states;
    Lexeme::LexemeString lexeme;
protected:
    void AddInstruction(const State& From, unsigned char symbol, const State& To);
    void AddTerminalState(const State& state);
public:
    Outcome operator() (unsigned char symbol);
    Outcome receive();
    void reset();
};

class SeparatorMachine: virtual public DFSMachine{
public:
    SeparatorMachine();
};

class NumberMachine: virtual public DFSMachine{
public:
    NumberMachine();
};

class NameMachine: virtual public DFSMachine{
public:
    NameMachine();
};

class StringMachine: virtual public DFSMachine{
public:
    StringMachine();
};

class UniversalMachine: public SeparatorMachine, public NumberMachine, public NameMachine, public StringMachine{
public:
    UniversalMachine();
};
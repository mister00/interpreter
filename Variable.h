#pragma once
#include "Type.h"
#include "Data.h"
#include "Operand.h"

class Variable {
private:
    bool const_qualifier;
    TypeID type_id;
    std::unique_ptr<Data> data;
public:
    Variable() = default;
    Variable(const Variable& other, bool minus = false);
    Variable(TypeID type_id, Data* data, bool const_qualifier = false, bool minus = false);
    template<typename T>
    Variable(TypeID type_id, T data, bool const_qualifier = false):
    type_id(type_id), data(new Data_store<T>(data)), const_qualifier(const_qualifier){}

    template <typename T>
    explicit operator T();
    Operand get_ptr_operand() const;
    bool is_const() const;
    TypeID get_type() const;
};

template <typename T>
Variable::operator T(){
    auto op = dynamic_cast<Data_store<T>*>(data.get());
    if (op == nullptr){
        throw std::logic_error("wrong type of data");
    } else {
        return op->get();
    }
}

using VariableID = long;

class VariableTable{
private:
    std::vector<Variable> table;
public:
    static const VariableID FALSE;
    static const VariableID TRUE;
    VariableID add_constant(TypeID type_id, Data* data, bool minus = false);
    VariableID add_constant(VariableID id, bool minus = false);
    VariableID add_variable(const Type& type, TypeID type_id);
    VariableTable();
    const Variable& at(long a) const;
};

class VariableNames: public std::unordered_map<std::string, VariableID>{
public:
    VariableNames();
};

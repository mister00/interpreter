#include <istream>
#include <stdexcept>
#include "Lexer.h"

void Lexer::assign(DFSMachine::Outcome outcome){
    if (std::get<0>(outcome) == DFSMachine::v_error){
        throw std::runtime_error("wrong lexeme, line " + std::to_string(line));
    } else if (std::get<0>(outcome) == DFSMachine::v_done) {
        lexeme.form(std::get<1>(outcome), line);
    }
}

void Lexer::next(){
    if (!good_stream){
        throw std::runtime_error("unexpected end of file");
    }
    int s = stream.get();
    DFSMachine::Outcome outcome(DFSMachine::v_next, "");
    while(stream.good() && std::get<0>(outcome) == DFSMachine::v_next){
        char c = static_cast<char>(s);
        outcome = machine(c);
        if (std::get<0>(outcome) == DFSMachine::v_next){
            s = stream.get();
        }
    }
    if (stream.good()){
        assign(outcome);
        stream.unget();
    } else if (stream.eof()){
        good_stream = false;
        outcome = machine.receive();
        assign(outcome);
    } else {
        throw std::runtime_error("bad stream");
    }
}

bool Lexer::good() const{
    return good_stream;
}

Lexeme Lexer::get(){
    if (ungetted){
        ungetted = false;
    } else {
        next();
    }
    return lexeme;
}

Lexeme Lexer::peek(){
    if (!ungetted) {
        next();
        ungetted = true;
    }
    return lexeme;
}

void Lexer::unget(){
    if (ungetted){
        throw std::logic_error("second unget");
    } else {
        ungetted = true;
    }
}
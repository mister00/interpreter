#include "Interpreter.h"

void Interpreter::interpret() {
    parser.parse();
    while (counter < chain.size()){
        long res = chain.at(counter)->execute(stack);
        if (res != -1){
            counter = res;
        } else {
            ++counter;
        }
    }
}
#pragma once
#include <vector>
#include <memory>
#include <stack>
#include <set>
#include "Lexer.h"
#include "Lexeme.h"
#include "Command.h"
#include "Type.h"
#include "Variable.h"

using Chain = std::vector<std::shared_ptr<Command>>;

class Parser{
public:
    using Label = long;
    using LabelTable = std::unordered_map<Label, long>;
private:
    Lexer lexer;
    Chain& chain;
    TypeTable type_table;
    TypeNames type_names;
    VariableTable& variable_table;
    VariableNames variable_names;
    LabelTable  label_table;
    std::set<std::string> taken_identifiers;
    const std::shared_ptr<Command> if_command;
    const std::shared_ptr<Command> goto_command;

    TypeID variable();
    TypeID factor();
    TypeID term();
    TypeID simple_expression();
    TypeID expression();
    void statement();
    VariableID constant();
    TypeID type();
    void block();
    void program();

    void assert_ident_unique(const std::string& string);
public:
    Parser(Chain& chain, VariableTable& variable_table, std::istream& stream):
        lexer(stream),
        chain(chain),
        variable_table(variable_table),
        if_command(new CommandIf),
        goto_command(new CommandGoto){}
    void parse();
};

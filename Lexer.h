#pragma once
#include "Lexeme.h"
#include "DFSMachine.h"

class Lexer{
private:
    UniversalMachine machine;
    std::istream& stream;
    Lexeme lexeme;
    int line = 0;
    bool good_stream = true;
    bool ungetted = false;
    void assign(DFSMachine::Outcome outcome);
    void next();
public:
    explicit Lexer(std::istream& stream): stream(stream){}
    bool good() const;
    Lexeme get();
    void unget();
    Lexeme peek();
};

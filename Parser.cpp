#include "Parser.h"
#include <set>

TypeID Parser::variable(){
    Lexeme lexeme = lexer.get();
    if (lexeme.get_type() == Lexeme::identifier){
        VariableID var_id;
        try{
            var_id = variable_names.at(lexeme.get_string());
        } catch (std::out_of_range&){
            throw std::runtime_error("not a name of a variable");
        }
        const Variable& var = variable_table.at(var_id);
        chain.emplace_back(new CommandPush(var.get_ptr_operand()));
        return var.get_type();
    } else {
        throw std::runtime_error("expected variable");
    }
}

TypeID Parser::factor(){
    Lexeme lexeme = lexer.get();
    switch (lexeme.get_type()){
        case Lexeme::identifier:
            lexer.unget();
            return variable();
        case Lexeme::nil_key:
        case Lexeme::int_const:
        case Lexeme::double_const:
        case Lexeme::char_const:
        case Lexeme::string_const:
            chain.emplace_back(new CommandPush(lexeme));
            return type_table.get_type(lexeme);
        case Lexeme::not_key: {
            TypeID type_id = factor();
            type_id = type_table.get_result_type(type_id, c_not);
            chain.emplace_back(type_table.get_command(type_id, c_not));
            return type_id;
        }
        case Lexeme::open_bracket: {
            TypeID type_id1 = expression();
            if (lexer.get().get_type() != Lexeme::close_bracket) {
                throw std::runtime_error("expected ')'");
            }
            return type_id1;
        }
        default:
            throw std::runtime_error("expected factor");
    }
}

TypeID Parser::term(){
    TypeID type_id1 = factor();
    Lexeme lexeme = lexer.get();
    Lexeme::LexemeType type = lexeme.get_type();
    while (type == Lexeme::mult || type == Lexeme::div_float || type == Lexeme::div_key || type == Lexeme::mod_key ||
            type == Lexeme::and_key){
        TypeID type_id2 = factor();
        chain.emplace_back(type_table.get_command(type_id1, type_id2, lexeme.get_operator()));
        type_id1 = type_table.get_result_type(type_id1, type_id2, lexeme.get_operator());
        lexeme = lexer.get();
        type = lexeme.get_type();
    }
    lexer.unget();
    return type_id1;
}

TypeID Parser::simple_expression(){
    Lexeme lexeme = lexer.get();
    TypeID type_id1;
    switch(lexeme.get_type()){
        case Lexeme::add:
            type_id1 = term();
            chain.emplace_back(type_table.get_command(type_id1, c_unary_plus));
            type_id1 = type_table.get_result_type(type_id1, c_unary_plus);
            break;
        case Lexeme::sub:
            type_id1 = term();
            chain.emplace_back(type_table.get_command(type_id1, c_oppos));
            type_id1 = type_table.get_result_type(type_id1, c_oppos);
            break;
        default:
            lexer.unget();
            type_id1 = term();
    }
    lexeme = lexer.get();
    Lexeme::LexemeType type = lexeme.get_type();
    while (type == Lexeme::add || type == Lexeme::sub || type == Lexeme::or_key){
        TypeID type_id2 = term();
        chain.emplace_back(type_table.get_command(type_id1, type_id2, lexeme.get_operator()));
        type_id1 = type_table.get_result_type(type_id1, type_id2, lexeme.get_operator());
        lexeme = lexer.get();
        type = lexeme.get_type();
    }
    lexer.unget();
    return type_id1;
}

TypeID Parser::expression(){
    TypeID type_id1 = simple_expression();
    Lexeme lexeme = lexer.get();
    switch(lexeme.get_type()){
        case Lexeme::less:
        case Lexeme::l_equal:
        case Lexeme::greater:
        case Lexeme::g_equal:
        case Lexeme::equal:
        case Lexeme::not_equal:
        case Lexeme::in_key: {
            TypeID type_id2 = simple_expression();
            chain.emplace_back(type_table.get_command(type_id1, type_id2, lexeme.get_operator()));
            type_id1 = type_table.get_result_type(type_id1, type_id2, lexeme.get_operator());
            return type_id1;
        }
        default:
            lexer.unget();
            return type_id1;
    }
}

void Parser::statement(){
    Lexeme lexeme = lexer.get();
    if (lexeme.get_type() == Lexeme::int_const){
        if (lexer.get().get_type() != Lexeme::colon){
            throw std::runtime_error("expected ':'");
        } else {
            lexeme = lexer.get();
        }
    }
    switch (lexeme.get_type()){
        case Lexeme::identifier: {
            lexer.unget();
            TypeID type_id1 = variable();
            lexeme = lexer.get();
            if (lexeme.get_type() == Lexeme::assign) { //add check if assigning to const
                TypeID type_id2 = expression();
                chain.emplace_back(type_table.get_command(type_id1, type_id2, c_assign));
                //} else if (lexeme.get_type() == Lexeme::open_bracket){
                //    //parameter list
                //    if (lexeme.get_type() != Lexeme::close_bracket){
                //        throw std::runtime_error("expected ')'");
                //    }
            } else {
                //lexer.unget();
                throw std::runtime_error("expected ':='");
            }
            break;
        }
        case Lexeme::begin_key:
            while (lexeme.get_type() != Lexeme::end_key){
                statement();
                lexeme = lexer.get();
                if (lexeme.get_type() != Lexeme::semicolon && lexeme.get_type() != Lexeme::end_key){
                    throw std::runtime_error("expected ';' or 'end'");
                }
            }
            break;
        case Lexeme::if_key: {
            if (!type_table.is_logical(expression())){
                throw std::runtime_error("'if' expression must be logical");
            }
            if (lexer.get().get_type() != Lexeme::then_key) {
                throw std::runtime_error("expected 'then'");
            }
            long reserved_place = chain.size();
            chain.emplace_back();
            chain.emplace_back(if_command);
            statement();
            chain[reserved_place].reset(new CommandPush(chain.size()));
            if (lexer.get().get_type() == Lexeme::else_key) {
                reserved_place = chain.size();
                chain.emplace_back();
                chain.emplace_back(goto_command);
                statement();
                chain[reserved_place].reset(new CommandPush(chain.size()));
            } else {
                lexer.unget();
            }
            break;
        }
        case Lexeme::while_key: {
            long cycle_place = chain.size();
            if (!type_table.is_logical(expression())){
                throw std::runtime_error("'while' expression must be logical");
            }
            if (lexer.get().get_type() != Lexeme::do_key) {
                throw std::runtime_error("expected 'do'");
            }
            long reserved_place = chain.size();
            chain.emplace_back();
            chain.emplace_back(if_command);
            statement();
            chain.emplace_back(new CommandPush(cycle_place));
            chain.emplace_back(goto_command);
            chain[reserved_place].reset(new CommandPush(chain.size()));
            break;
        }
        case Lexeme::repeat_key: {
            long cycle_place = chain.size();
            while (lexeme.get_type() != Lexeme::until_key) {
                expression();
                lexeme = lexer.get();
                if (lexeme.get_type() != Lexeme::semicolon && lexeme.get_type() != Lexeme::until_key) {
                    throw std::runtime_error("expected ';' or 'until'");
                }
            }
            if (!type_table.is_logical(expression())){
                throw std::runtime_error("'until' expression must be logical");
            }
            chain.emplace_back(new CommandPush(cycle_place));
            chain.emplace_back(if_command);
            break;
        }
        case Lexeme::for_key: //not implemented
            lexeme = lexer.get();
            if (lexeme.get_type() != Lexeme::identifier){
                throw std::runtime_error("expected identifier");
            }
            if (lexeme.get_type() != Lexeme::assign){
                throw std::runtime_error("expected ':='");
            }
            expression();
            lexeme = lexer.get();
            if (lexeme.get_type() != Lexeme::to_key && lexeme.get_type() != Lexeme::downto_key){
                throw std::runtime_error("expected 'to' or 'downto'");
            }
            expression();
            if (lexeme.get_type() != Lexeme::do_key){
                throw std::runtime_error("expected 'do'");
            }
            statement();
            break;
        case Lexeme::goto_key:
            lexeme = lexer.get();
            if (lexeme.get_type() != Lexeme::int_const){
                throw std::runtime_error("expected unsigned int");
            }
            chain.emplace_back(new CommandPush(lexeme.get_int()));
            chain.emplace_back(goto_command);
            break;
        default:
            lexer.unget();
    }
}


VariableID Parser::constant(){
    bool minus = false;
    bool sign = false;
    Lexeme lexeme = lexer.get();
    if (lexeme.get_type() == Lexeme::sub){
        minus = true;
        sign = true;
        lexeme = lexer.get();
    } else if (lexeme.get_type() == Lexeme::add) {
        sign = true;
        lexeme = lexer.get();
    }
    if (lexeme.get_type() == Lexeme::identifier){
        try{
            return variable_table.add_constant(variable_names.at(lexeme.get_string()), minus);
        } catch (std::out_of_range&){
            throw std::runtime_error("unknown identifier");
        }
    } else if (lexeme.get_type() == Lexeme::int_const || lexeme.get_type() == Lexeme::double_const){
        return variable_table.add_constant(type_table.get_type(lexeme), lexeme.get_data(), minus);
    } else if (lexeme.get_type() == Lexeme::char_const || lexeme.get_type() == Lexeme::string_const){
        if (sign){
            throw std::runtime_error("expected number constant");
        }
        return variable_table.add_constant(type_table.get_type(lexeme), lexeme.get_data(), minus);
    } else {
        throw std::runtime_error("expected constant");
    }
}

TypeID Parser::type(){
    Lexeme lexeme = lexer.get();
    if (lexeme.get_type() != Lexeme::identifier){
        throw std::runtime_error("expected identifier");
    }
    try{
        return type_names.at(lexeme.get_string());
    } catch (std::out_of_range&){
        throw std::runtime_error("unknown identifier");
    }
}

void Parser::block(){
    Lexeme lexeme = lexer.get();
    if (lexeme.get_type() == Lexeme::label_key){
        while (lexeme.get_type() != Lexeme::semicolon){
            lexeme = lexer.get();
            if (lexeme.get_type() != Lexeme::int_const) {
                throw std::runtime_error("expected integer");
            }
            label_table[lexeme.get_int()];
            lexeme = lexer.get();
            if (lexeme.get_type() != Lexeme::semicolon && lexeme.get_type() != Lexeme::comma){
                throw std::runtime_error("expected ',' or ';'n");
            }
        }
        lexeme = lexer.get();
    }
    if (lexeme.get_type() == Lexeme::const_key){
        lexeme = lexer.get();
        if (lexeme.get_type() != Lexeme::identifier) {
            throw std::runtime_error("expected identifier");
        }
        do {
            std::string name = lexeme.get_string();
            lexeme = lexer.get();
            if (lexeme.get_type() != Lexeme::equal) {
                throw std::runtime_error("expected '='");
            }
            assert_ident_unique(name);
            variable_names[name] = constant();
            taken_identifiers.insert(name);
            lexeme = lexer.get();
            if (lexeme.get_type() != Lexeme::semicolon){
                throw std::runtime_error("expected ';'");
            }
            lexeme = lexer.get();
        } while (lexeme.get_type() == Lexeme::identifier);
    }
    if (lexeme.get_type() == Lexeme::type_key){
        lexeme = lexer.get();
        if (lexeme.get_type() != Lexeme::identifier) {
            throw std::runtime_error("expected identifier");
        }
        do {
            std::string name = lexeme.get_string();
            lexeme = lexer.get();
            if (lexeme.get_type() != Lexeme::equal) {
                throw std::runtime_error("expected '='");
            }
            assert_ident_unique(name);
            type_names[name] = type();
            lexeme = lexer.get();
            if (lexeme.get_type() != Lexeme::semicolon){
                throw std::runtime_error("expected ';'");
            }
            lexeme = lexer.get();
        } while (lexeme.get_type() == Lexeme::identifier);
    }
    if (lexeme.get_type() == Lexeme::var_key){
        lexeme = lexer.get();
        if (lexeme.get_type() != Lexeme::identifier) {
            throw std::runtime_error("expected identifier");
        }
        do {
            std::set<std::string> names;
            names.insert(lexeme.get_string());
            lexeme = lexer.get();
            while (lexeme.get_type() == Lexeme::comma){
                lexeme = lexer.get();
                if (lexeme.get_type() != Lexeme::identifier) {
                    throw std::runtime_error("expected identifier");
                }
                names.insert(lexeme.get_string());
                lexeme = lexer.get();
            }
            if (lexeme.get_type() != Lexeme::colon){
                throw std::runtime_error("expected ',' or ':'");
            }
            TypeID type_id = type();
            for (const std::string& name : names){
                assert_ident_unique(name);
                variable_names[name] = variable_table.add_variable(type_table.at(type_id), type_id);
            }
            lexeme = lexer.get();
            if (lexeme.get_type() != Lexeme::semicolon){
                throw std::runtime_error("expected ';'");
            }
            lexeme = lexer.get();
        } while (lexeme.get_type() == Lexeme::identifier);
    }
    //functions
    if (lexeme.get_type() == Lexeme::begin_key){
        while (lexeme.get_type() != Lexeme::end_key){
            statement();
            lexeme = lexer.get();
            if (lexeme.get_type() != Lexeme::semicolon && lexeme.get_type() != Lexeme::end_key){
                throw std::runtime_error("expected 'end' or ';'");
            }
        }
    }
}

void Parser::program(){
    Lexeme lexeme = lexer.get();
    if (lexeme.get_type() == Lexeme::program_key){
        lexeme = lexer.get();
        if (lexeme.get_type() != Lexeme::identifier){
            throw std::runtime_error("expected identifier");
        }
        //parameter list
        lexeme = lexer.get();
        if (lexeme.get_type() != Lexeme::semicolon){
            throw std::runtime_error("expected ';'");
        }
    } else {
        lexer.unget();
    }
    block();
    lexeme = lexer.get();
    if (lexeme.get_type() != Lexeme::dot){
        throw std::runtime_error("expected '.'");
    }
}

void Parser::assert_ident_unique(const std::string& string) {
    if (taken_identifiers.find(string) != taken_identifiers.end()){
        throw std::runtime_error("existing identifier");
    }
}

void Parser::parse(){
    program();
}
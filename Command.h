#pragma once
#include <stack>
#include "Operand.h"
#include "Lexeme.h"

using Stack = std::stack<Operand>;

class Command{
public:
    virtual ~Command() = default;
    virtual long execute(Stack& stack) const = 0;
    virtual Command* copy() const = 0;
};

class CommandPush: public Command{
private:
    Operand operand;
public:
    CommandPush() = default;
    explicit CommandPush(const Lexeme& lexeme){
        switch (lexeme.get_type()){
            case Lexeme::int_const:
                operand = Operand(lexeme.get_int());
                break;
            case Lexeme::char_const:
                operand = Operand(lexeme.get_char());
                break;
            case Lexeme::string_const:
                operand = Operand(lexeme.get_string());
                break;
            case Lexeme::double_const:
                operand = Operand(lexeme.get_double());
                break;
            default:
                throw std::logic_error("can push only const lexeme");
        }
    }
    explicit CommandPush(const Operand& operand): operand(operand){}
    explicit CommandPush(long num): operand(num){}
    long execute(Stack& stack) const override {
        stack.push(operand);
        return -1;
    }
    Command* copy() const override {
        return new CommandPush(*this);
    }
};

template<typename T1, typename T2>
class CommandAdd: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 + op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandAdd<T1, T2>();
    }
};

template<>
class CommandAdd<char, char>: public Command{
    long execute(Stack& stack) const override {
        auto op2 = static_cast<char>(stack.top());
        stack.pop();
        auto op1 = static_cast<char>(stack.top());
        stack.pop();
        stack.push(Operand(std::string() + op1 + op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandAdd<char, char>();
    }
};

template<typename T1, typename T2>
class CommandSub: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 - op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandSub<T1, T2>();
    }
};

template<typename T1>
class CommandOppos: public Command{
    long execute(Stack& stack) const override {
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(-op1));
        return -1;
    }
    Command* copy() const override {
        return new CommandOppos<T1>();
    }
};

template<typename T1>
class CommandUnaryPlus: public Command{
    long execute(Stack& stack) const override {
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(+op1));
        return -1;
    }
    Command* copy() const override {
        return new CommandUnaryPlus<T1>();
    }
};

template<typename T1, typename T2>
class CommandMult: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 * op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandMult<T1, T2>();
    }
};

template<typename T1, typename T2>
class CommandDivReal: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 / op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandDivReal<T1, T2>();
    }
};

template<>
class CommandDivReal<long, long>: public Command{
    long execute(Stack& stack) const override {
        auto op2 = static_cast<double>(stack.top());
        stack.pop();
        auto op1 = static_cast<double>(stack.top());
        stack.pop();
        stack.push(Operand(op1 / op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandDivReal<long, long>();
    }
};

template<typename T1, typename T2>
class CommandDivInt: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 / op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandDivInt<T1, T2>();
    }
};

template<typename T1, typename T2>
class CommandMod: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 % op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandMod<T1, T2>();
    }
};

template<typename T1>
class CommandNot: public Command{
    long execute(Stack& stack) const override {
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(!op1));
        return -1;
    }
    Command* copy() const override {
        return new CommandNot<T1>();
    }
};

template<typename T1, typename T2>
class CommandAnd: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 && op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandAnd<T1, T2>();
    }
};

template<typename T1, typename T2>
class CommandOr: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 || op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandOr<T1, T2>();
    }
};

template<typename T1, typename T2>
class CommandGreater: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 > op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandGreater<T1, T2>();
    }
};

template<typename T1, typename T2>
class CommandGEqual: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 >= op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandGEqual<T1, T2>();
    }
};

template<typename T1, typename T2>
class CommandEqual: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 == op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandEqual<T1, T2>();
    }
};

template<typename T1, typename T2>
class CommandLess: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 < op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandLess<T1, T2>();
    }
};


template<typename T1, typename T2>
class CommandLEqual: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 <= op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandLEqual<T1, T2>();
    }
};

template<typename T1, typename T2>
class CommandNotEqual: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1 op1 = static_cast<T1>(stack.top());
        stack.pop();
        stack.push(Operand(op1 != op2));
        return -1;
    }
    Command* copy() const override {
        return new CommandNotEqual<T1, T2>();
    }
};

template <typename T1, typename T2>
class CommandAssign: public Command{
    long execute(Stack& stack) const override {
        T2 op2 = static_cast<T2>(stack.top());
        stack.pop();
        T1* op1 = stack.top().get_ptr<T1>();
        stack.pop();
        *op1 = op2;
        return -1;
    }
    Command* copy() const override {
        return new CommandAssign<T1, T2>();
    }
};

class CommandGoto: public Command{
    long execute(Stack& stack) const override {
        long op1 = static_cast<long>(stack.top());
        stack.pop();
        return op1;
    }
    Command* copy() const override {
        return new CommandGoto();
    }
};

class CommandIf: public Command{
    long execute(Stack& stack) const override {
        long op2 = static_cast<long>(stack.top());
        stack.pop();
        bool op1 = static_cast<bool>(stack.top());
        stack.pop();
        if (!op1) {
            return op2;
        } else {
            return -1;
        }
    }
    Command* copy() const override {
        return new CommandIf();
    }
};
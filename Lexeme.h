#pragma once
#include <string>
#include "Data.h"

enum OperatorType{
    c_add,
    c_sub,
    c_oppos,
    c_unary_plus,
    c_mult,
    c_div_float,
    c_div_int,
    c_mod,
    c_and,
    c_or,
    c_not,
    c_assign,
    c_greater,
    c_g_equal,
    c_equal,
    c_less,
    c_l_equal,
    c_not_equal,
    c_in,
};

class Lexeme{
public:
    using LexemeString = std::string;
    enum LexemeType{
        add,
        sub,
        mult,
        div_float,
        open_bracket,
        close_bracket,
        open_sq_bracket,
        close_sq_bracket,
        dot,
        comma,
        colon,
        assign,
        greater,
        g_equal,
        equal,
        less,
        l_equal,
        not_equal,
        semicolon,
        arrow_up,
        and_key,
        array_key,
        begin_key,
        case_key,
        const_key,
        div_key,
        do_key,
        downto_key,
        else_key,
        end_key,
        file_key,
        for_key,
        function_key,
        goto_key,
        if_key,
        in_key,
        label_key,
        mod_key,
        nil_key,
        not_key,
        of_key,
        or_key,
        packed_key,
        procedure_key,
        program_key,
        record_key,
        repeat_key,
        set_key,
        then_key,
        to_key,
        type_key,
        until_key,
        var_key,
        while_key,
        with_key,
        int_const,
        double_const,
        string_const,
        char_const,
        identifier,
    };
private:
    LexemeType lexeme_type;
    LexemeString data;
    int line;
public:
    LexemeType get_type() const;
    std::string get_string() const;
    double get_double() const;
    long get_int() const;
    char get_char() const;
    Data* get_data() const;
    OperatorType get_operator() const;
    Lexeme() = default;
    Lexeme(const LexemeString& string, int line_);
    void form(const LexemeString& string, int line_);
};

#pragma once
#include "Data.h"

class Operand{
public:
    enum OperandForm{
        f_address,
        f_value,
    };
private:
    OperandForm form;
    std::unique_ptr<Data> data;
public:
    Operand() = default;
    Operand(const Operand& other): form(other.form), data(other.data->copy()){}
    explicit Operand(Data* data, OperandForm form = f_value): form(form), data(data){}
    template <typename T>
    explicit Operand(T data, OperandForm form = f_value): form(form), data(new Data_store<T>(data)){}
    Operand& operator= (const Operand& other);
    template <typename T>
    explicit operator T() const;
    template <typename T>
    T* get_ptr() const;
};

template <typename T>
Operand::operator T() const{
    if (form == f_value) {
        auto op = dynamic_cast<Data_store<T>*>(data.get());
        if (op == nullptr) {
            throw std::logic_error("illegal attempt to get type from operand");
        } else {
            return op->get();
        }
    } else {
        auto op = dynamic_cast<Data_ptr<T*>*>(data.get());
        if (op == nullptr) {
            throw std::logic_error("illegal attempt to get type from operand");
        } else {
            return *(op->get());
        }
    };
}

template <typename T>
T* Operand::get_ptr() const {
    if (form != f_address){
        throw std::logic_error("can't cast operand value to ptr");
    }
    auto op = dynamic_cast<Data_ptr<T*>*>(data.get());
    if (op == nullptr){
        throw std::logic_error("illegal attempt to get type from operand");
    } else {
        return op->get();
    }
}
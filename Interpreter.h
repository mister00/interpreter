#pragma once
#include "Parser.h"
#include "Operand.h"

class Interpreter {
public:
    using Stack = std::stack<Operand>;
private:
    Chain chain;
    VariableTable variable_table;
    Parser parser;
    Stack stack;
    long counter = 0;
public:
    Interpreter(std::istream& stream): parser(chain, variable_table, stream){}
    void interpret();
};

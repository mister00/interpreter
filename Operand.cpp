#include "Operand.h"

Operand& Operand::operator= (const Operand& other){
    form = other.form;
    data.reset(other.data->copy());
    return *this;
}
#include "DFSMachine.h"
#include <vector>

DFSMachine::Outcome DFSMachine::operator() (unsigned char symbol){
    if (instructions.find(Action(state, symbol)) == instructions.end()){
        return receive();
    } else {
        state = instructions[Action(state, symbol)];
        if (state == "start"){
            lexeme.clear();
        } else {
            lexeme.push_back(symbol);
        }
        return Outcome(v_next, Lexeme::LexemeString());
    }
}

void DFSMachine::AddInstruction(const State& from, unsigned char symbol, const State& to){
    instructions[Action(from, symbol)] = to;
}

void DFSMachine::AddTerminalState(const State& new_state){
    terminal_states.insert(new_state);
}

DFSMachine::Outcome DFSMachine::receive(){
    Outcome outcome;
    if (terminal_states.find(state) == terminal_states.end()){
        outcome =  Outcome(v_error, lexeme);
    } else {
        outcome =  Outcome(v_done, lexeme);
    }
    this->reset();
    return outcome;
}

void DFSMachine::reset(){
    state = "start";
    lexeme.clear();
}

SeparatorMachine::SeparatorMachine(){
    const std::vector<std::string> SEPARATOR_LIST =
            {"=", ">", ">=", "<", "<=", "<>", ":=", "+", "-", "*", "/", "(", ")", "[", "]", "^", ";", ":", ".", ","};
    for (auto separator = SEPARATOR_LIST.cbegin(); separator != SEPARATOR_LIST.cend(); ++separator){
        AddTerminalState(*separator);
        AddInstruction("start", (*separator)[0], separator->substr(0, 1));
        for (int i = 1; i < separator->size(); ++i){
            AddInstruction(separator->substr(0, i), (*separator)[i], separator->substr(0, i+1));
        }
    }
}

NumberMachine::NumberMachine(){
    for (unsigned char c = '0'; c <= '9'; c++){
        AddInstruction("start", c, "int");
        AddInstruction("int", c, "int");
    }
    AddTerminalState("int");
    AddInstruction("int", '.', "int_with_point");
    for (unsigned char c = '0'; c <= '9'; c++) {
        AddInstruction("int_with_point", c, "real");
        AddInstruction("real", c, "real");
    }
    AddTerminalState("real");
    AddInstruction("int", 'E', "real_with_e");
    AddInstruction("real", 'E', "real_with_e");
    AddInstruction("real_with_e", '+', "real_with_e_sign");
    AddInstruction("real_with_e", '-', "real_with_e_sign");
    for (unsigned char c = '0'; c <= '9'; c++){
        AddInstruction("real_with_e", c, "real_exp");
        AddInstruction("real_with_e_sign", c, "real_exp");
        AddInstruction("real_exp", c, "real_exp");
    }
    AddInstruction("real_exp", '0', "real_exp");
    AddTerminalState("real_exp");
}

NameMachine::NameMachine(){
    for (unsigned char c = 'a'; c <= 'z'; c++) {
        AddInstruction("start", c, "name");
        AddInstruction("name", c, "name");
    }
    for (unsigned char c = 'A'; c <= 'Z'; c++) {
        AddInstruction("start", c, "name");
        AddInstruction("name", c, "name");
    }
    AddInstruction("start", '_', "name");
    AddInstruction("name", '_', "name");
    for (unsigned char c = '0'; c <= '9'; c++){
        AddInstruction("name", c, "name");
    }
    AddTerminalState("name");
}

StringMachine::StringMachine(){
    AddInstruction("start", '\'', "open_string");
    for (unsigned char c = 0; c < '\''; c++) {
        AddInstruction("open_string", c, "open_string");
    }
    for (unsigned char c = '\'' + 1; c < 255; c++) {
        AddInstruction("open_string", c, "open_string");
    }
    AddInstruction("open_string", '\'', "string");
    AddTerminalState("string");
}

UniversalMachine::UniversalMachine(){
    AddInstruction("start", ' ', "start");
    AddInstruction("start", '\n', "start");
    AddInstruction("start", '{', "comment");
    for (unsigned char c = 0; c < '}'; c++) {
        AddInstruction("comment", c, "comment");
    }
    for (unsigned char c = '}' + 1; c < 255; c++) {
        AddInstruction("comment", c, "comment");
    }
    AddInstruction("comment", '}', "start");
}
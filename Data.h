#pragma once
#include <string>
#include <memory>

class Data{
public:
    virtual ~Data() = default;
    virtual Data* copy() const = 0;
    virtual Data* get_ptr_copy() = 0;
};

template <typename T>
class Data_store: public Data{
private:
    T data;
public:
    Data_store(const T& data): data(data){}
    Data_store() = default;
    Data* copy() const override;
    Data* get_ptr_copy() override;
    T get() const;
};

template<typename T>
class Data_ptr: public Data{
private:
    T data;
public:
    Data_ptr(const T& data): data(data){}
    Data_ptr() = default;
    Data* copy() const override;
    Data* get_ptr_copy() override;
    T get() const;
};

template <typename T>
Data* Data_store<T>::copy() const{
    return new Data_store<T>(data);
}

template <typename T>
Data* Data_store<T>::get_ptr_copy(){
    return new Data_ptr<T*>(&data);
}

template <typename T>
T Data_store<T>::get() const{
    return data;
}

template <typename T>
Data* Data_ptr<T>::copy() const {
    return new Data_ptr<T>(data);
}

template <typename T>
Data* Data_ptr<T>::get_ptr_copy(){
    throw std::logic_error("can't get ptr of ptr");
}

template <typename T>
T Data_ptr<T>::get() const{
    return data;
}
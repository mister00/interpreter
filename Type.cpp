#include "Type.h"

const long TypeTable::INT = 0;
const long TypeTable::DOUBLE = 1;
const long TypeTable::BOOL = 2;
const long TypeTable::CHAR = 3;
const long TypeTable::STRING = 4;

Type::TypeNature Type::get_nature() const {
    return nature;
}

TypeNames::TypeNames():
    std::unordered_map<std::string, TypeID> {
            {"integer", TypeTable::INT},
            {"real", TypeTable::DOUBLE},
            {"boolean", TypeTable::BOOL},
            {"char", TypeTable::CHAR},
            {"string", TypeTable::STRING},
    }
{}

void TypeTable::add_operator(TypeID a, TypeID b, OperatorType op, TypeID c){
    operator_table[std::make_tuple(a, b, op)] = c;
}

void TypeTable::add_command(TypeID a, TypeID b, OperatorType op, Command* c) {
    command_table[std::make_tuple(a, b, op)].reset(c);
}

TypeTable::TypeTable(){
    table.emplace_back(Type::t_int);
    table.emplace_back(Type::t_double);
    table.emplace_back(Type::t_bool);
    table.emplace_back(Type::t_char);
    table.emplace_back(Type::t_string);

    for (OperatorType c : {c_add, c_sub, c_mult}){
        add_operator(INT, INT, c, INT);
        add_operator(DOUBLE, DOUBLE, c, DOUBLE);
        add_operator(INT, DOUBLE, c, DOUBLE);
        add_operator(DOUBLE, INT, c, DOUBLE);
    }
    add_command(INT, INT, c_add, new CommandAdd<long, long>);
    add_command(DOUBLE, DOUBLE, c_add, new CommandAdd<double, double>);
    add_command(INT, DOUBLE, c_add, new CommandAdd<long, double>);
    add_command(DOUBLE, INT, c_add, new CommandAdd<double, long>);

    add_command(INT, INT, c_sub, new CommandSub<long, long>);
    add_command(DOUBLE, DOUBLE, c_sub, new CommandSub<double, double>);
    add_command(INT, DOUBLE, c_sub, new CommandSub<long, double>);
    add_command(DOUBLE, INT, c_sub, new CommandSub<double, long>);

    add_command(INT, INT, c_mult, new CommandMult<long, long>);
    add_command(DOUBLE, DOUBLE, c_mult, new CommandMult<double, double>);
    add_command(INT, DOUBLE, c_mult, new CommandMult<long, double>);
    add_command(DOUBLE, INT, c_mult, new CommandMult<double, long>);


    add_operator(STRING, STRING, c_add, STRING);
    add_operator(STRING, CHAR, c_add, STRING);
    add_operator(CHAR, STRING, c_add, STRING);
    add_operator(CHAR, CHAR, c_add, STRING);

    add_command(STRING, STRING, c_add, new CommandAdd<std::string, std::string>);
    add_command(STRING, CHAR, c_add, new CommandAdd<std::string, char>);
    add_command(CHAR, STRING, c_add, new CommandAdd<char, std::string>);
    add_command(CHAR, CHAR, c_add, new CommandAdd<char, char>);

    add_operator(INT, INT, c_div_float, DOUBLE);
    add_operator(DOUBLE, DOUBLE, c_div_float, DOUBLE);
    add_operator(INT, DOUBLE, c_div_float, DOUBLE);
    add_operator(DOUBLE, INT, c_div_float, DOUBLE);

    add_command(INT, INT, c_div_float, new CommandDivReal<long, long>);
    add_command(DOUBLE, DOUBLE, c_div_float, new CommandDivReal<double, double>);
    add_command(INT, DOUBLE, c_div_float, new CommandDivReal<long, double>);
    add_command(DOUBLE, INT, c_div_float, new CommandDivReal<double, long>);

    add_operator(INT, INT, c_div_int, INT);
    add_operator(INT, INT, c_mod, INT);

    add_command(INT, INT, c_div_int, new CommandDivInt<long, long>);
    add_command(INT, INT, c_mod, new CommandMod<long, long>);

    add_operator(INT, INT, c_oppos, INT);
    add_operator(DOUBLE, DOUBLE, c_oppos, DOUBLE);
    add_operator(INT, INT, c_unary_plus, INT);
    add_operator(DOUBLE, DOUBLE, c_unary_plus, DOUBLE);

    add_command(INT, INT, c_oppos, new CommandOppos<long>);
    add_command(DOUBLE, DOUBLE, c_oppos, new CommandOppos<double>);
    add_command(INT, INT, c_unary_plus, new CommandUnaryPlus<long>);
    add_command(DOUBLE, DOUBLE, c_unary_plus, new CommandUnaryPlus<double>);

    for (OperatorType c : {c_and, c_or, c_not}){
        add_operator(BOOL, BOOL, c, BOOL);
    }

    add_command(BOOL, BOOL, c_and, new CommandAnd<bool, bool>);
    add_command(BOOL, BOOL, c_or, new CommandOr<bool, bool>);
    add_command(BOOL, BOOL, c_not, new CommandNot<bool>);

    for (OperatorType c : {c_less, c_l_equal, c_equal,
                           c_greater, c_g_equal, c_not_equal}){
        add_operator(INT, INT, c, BOOL);
        add_operator(DOUBLE, DOUBLE, c, BOOL);
        add_operator(INT, DOUBLE, c, BOOL);
        add_operator(DOUBLE, INT, c, BOOL);
        add_operator(BOOL, BOOL, c, BOOL);
        add_operator(CHAR, CHAR, c, BOOL);
        add_operator(STRING, STRING, c, BOOL);
        add_operator(CHAR, STRING, c, BOOL);
        add_operator(STRING, CHAR, c, BOOL);
    }

    add_command(INT, INT, c_less, new CommandLess<long, long>);
    add_command(DOUBLE, DOUBLE, c_less, new CommandLess<double, double>);
    add_command(INT, DOUBLE, c_less, new CommandLess<long, double>);
    add_command(DOUBLE, INT, c_less, new CommandLess<double, long>);
    add_command(BOOL, BOOL, c_less, new CommandLess<bool, bool>);
    add_command(CHAR, CHAR, c_less, new CommandLess<char, char>);
    add_command(STRING, STRING, c_less, new CommandLess<std::string, std::string>);
    add_command(CHAR, STRING, c_less, new CommandLess<char, std::string>);
    add_command(STRING, CHAR, c_less, new CommandLess<std::string, char>);

    add_command(INT, INT, c_l_equal, new CommandLEqual<long, long>);
    add_command(DOUBLE, DOUBLE, c_l_equal, new CommandLEqual<double, double>);
    add_command(INT, DOUBLE, c_l_equal, new CommandLEqual<long, double>);
    add_command(DOUBLE, INT, c_l_equal, new CommandLEqual<double, long>);
    add_command(BOOL, BOOL, c_l_equal, new CommandLEqual<bool, bool>);
    add_command(CHAR, CHAR, c_l_equal, new CommandLEqual<char, char>);
    add_command(STRING, STRING, c_l_equal, new CommandLEqual<std::string, std::string>);
    add_command(CHAR, STRING, c_l_equal, new CommandLEqual<char, std::string>);
    add_command(STRING, CHAR, c_l_equal, new CommandLEqual<std::string, char>);

    add_command(INT, INT, c_equal, new CommandEqual<long, long>);
    add_command(DOUBLE, DOUBLE, c_equal, new CommandEqual<double, double>);
    add_command(INT, DOUBLE, c_equal, new CommandEqual<long, double>);
    add_command(DOUBLE, INT, c_equal, new CommandEqual<double, long>);
    add_command(BOOL, BOOL, c_equal, new CommandEqual<bool, bool>);
    add_command(CHAR, CHAR, c_equal, new CommandEqual<char, char>);
    add_command(STRING, STRING, c_equal, new CommandEqual<std::string, std::string>);
    add_command(CHAR, STRING, c_equal, new CommandEqual<char, std::string>);
    add_command(STRING, CHAR, c_equal, new CommandEqual<std::string, char>);

    add_command(INT, INT, c_greater, new CommandGreater<long, long>);
    add_command(DOUBLE, DOUBLE, c_greater, new CommandGreater<double, double>);
    add_command(INT, DOUBLE, c_greater, new CommandGreater<long, double>);
    add_command(DOUBLE, INT, c_greater, new CommandGreater<double, long>);
    add_command(BOOL, BOOL, c_greater, new CommandGreater<bool, bool>);
    add_command(CHAR, CHAR, c_greater, new CommandGreater<char, char>);
    add_command(STRING, STRING, c_greater, new CommandGreater<std::string, std::string>);
    add_command(CHAR, STRING, c_greater, new CommandGreater<char, std::string>);
    add_command(STRING, CHAR, c_greater, new CommandGreater<std::string, char>);

    add_command(INT, INT, c_g_equal, new CommandGEqual<long, long>);
    add_command(DOUBLE, DOUBLE, c_g_equal, new CommandGEqual<double, double>);
    add_command(INT, DOUBLE, c_g_equal, new CommandGEqual<long, double>);
    add_command(DOUBLE, INT, c_g_equal, new CommandGEqual<double, long>);
    add_command(BOOL, BOOL, c_g_equal, new CommandGEqual<bool, bool>);
    add_command(CHAR, CHAR, c_g_equal, new CommandGEqual<char, char>);
    add_command(STRING, STRING, c_g_equal, new CommandGEqual<std::string, std::string>);
    add_command(CHAR, STRING, c_g_equal, new CommandGEqual<char, std::string>);
    add_command(STRING, CHAR, c_g_equal, new CommandGEqual<std::string, char>);

    add_command(INT, INT, c_not_equal, new CommandNotEqual<long, long>);
    add_command(DOUBLE, DOUBLE, c_not_equal, new CommandNotEqual<double, double>);
    add_command(INT, DOUBLE, c_not_equal, new CommandNotEqual<long, double>);
    add_command(DOUBLE, INT, c_not_equal, new CommandNotEqual<double, long>);
    add_command(BOOL, BOOL, c_not_equal, new CommandNotEqual<bool, bool>);
    add_command(CHAR, CHAR, c_not_equal, new CommandNotEqual<char, char>);
    add_command(STRING, STRING, c_not_equal, new CommandNotEqual<std::string, std::string>);
    add_command(CHAR, STRING, c_not_equal, new CommandNotEqual<char, std::string>);
    add_command(STRING, CHAR, c_not_equal, new CommandNotEqual<std::string, char>);

    add_command(INT, INT, c_assign, new CommandAssign<long, long>);
    add_command(DOUBLE, DOUBLE, c_assign, new CommandAssign<double, double>);
    add_command(DOUBLE, INT, c_assign, new CommandAssign<double, long>);
    add_command(BOOL, BOOL, c_assign, new CommandAssign<bool, bool>);
    add_command(CHAR, CHAR, c_assign, new CommandAssign<char, char>);
    add_command(STRING, STRING, c_assign, new CommandAssign<std::string, std::string>);
    add_command(STRING, CHAR, c_assign, new CommandAssign<std::string, char>);
}

TypeID TypeTable::get_result_type(TypeID id1, TypeID id2, OperatorType command) {
    try {
        return operator_table.at(std::make_tuple(id1, id2, command));
    } catch (std::out_of_range&){
        throw std::runtime_error("can't apply operator to types");
    }
}

TypeID TypeTable::get_result_type(TypeID id1, OperatorType command) {
    try {
        return operator_table.at(std::make_tuple(id1, id1, command));
    } catch (std::out_of_range&){
        throw std::runtime_error("can't apply operator to types");
    }
}

std::shared_ptr<Command>& TypeTable::get_command(TypeID type1, TypeID type2, OperatorType op) {
    try {
        return command_table.at(Operator(type1, type2, op));
    } catch (std::out_of_range&){
        throw std::runtime_error("can't apply operator to types");
    }
}

std::shared_ptr<Command>& TypeTable::get_command(TypeID type, OperatorType op) {
    try{
        return command_table.at(Operator(type, type, op));
    } catch (std::out_of_range&){
        throw std::runtime_error("can't apply operator to types");
    }
}

TypeID TypeTable::get_type(const Lexeme& lexeme){
    switch (lexeme.get_type()) {
        case Lexeme::int_const:
            return INT;
        case Lexeme::double_const:
            return DOUBLE;
        case Lexeme::char_const:
            return CHAR;
        case Lexeme::string_const:
            return STRING;
        default:
            throw std::logic_error("can get type only from const lexeme");
    }
}

bool TypeTable::is_logical(TypeID type_id) {
    return type_id == BOOL;
}

const Type& TypeTable::at (long a) const{
    return table.at(a);
}

namespace std{
    bool operator==(const std::string& string, char c){
        return string == std::string() + c;
    }
    bool operator>=(const std::string& string, char c){
        return string >= std::string() + c;
    }
    bool operator>(const std::string& string, char c){
        return string > std::string() + c;
    }
    bool operator!=(const std::string& string, char c){
        return string != std::string() + c;
    }
    bool operator<=(const std::string& string, char c){
        return string <= std::string() + c;
    }
    bool operator<(const std::string& string, char c){
        return string < std::string() + c;
    }
    bool operator==(char c, const std::string& string){
        return string == std::string() + c;
    }
    bool operator>=(char c, const std::string& string){
        return string <= std::string() + c;
    }
    bool operator>(char c, const std::string& string){
        return string < std::string() + c;
    }
    bool operator!=(char c, const std::string& string){
        return string != std::string() + c;
    }
    bool operator<=(char c, const std::string& string){
        return string >= std::string() + c;
    }
    bool operator<(char c, const std::string& string){
        return string > std::string() + c;
    }
}
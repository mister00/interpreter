# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/nikita/interpreter/DFSMachine.cpp" "/home/nikita/interpreter/cmake-build-debug/CMakeFiles/interpreter.dir/DFSMachine.cpp.o"
  "/home/nikita/interpreter/Interpreter.cpp" "/home/nikita/interpreter/cmake-build-debug/CMakeFiles/interpreter.dir/Interpreter.cpp.o"
  "/home/nikita/interpreter/Lexeme.cpp" "/home/nikita/interpreter/cmake-build-debug/CMakeFiles/interpreter.dir/Lexeme.cpp.o"
  "/home/nikita/interpreter/Lexer.cpp" "/home/nikita/interpreter/cmake-build-debug/CMakeFiles/interpreter.dir/Lexer.cpp.o"
  "/home/nikita/interpreter/Operand.cpp" "/home/nikita/interpreter/cmake-build-debug/CMakeFiles/interpreter.dir/Operand.cpp.o"
  "/home/nikita/interpreter/Parser.cpp" "/home/nikita/interpreter/cmake-build-debug/CMakeFiles/interpreter.dir/Parser.cpp.o"
  "/home/nikita/interpreter/Type.cpp" "/home/nikita/interpreter/cmake-build-debug/CMakeFiles/interpreter.dir/Type.cpp.o"
  "/home/nikita/interpreter/Variable.cpp" "/home/nikita/interpreter/cmake-build-debug/CMakeFiles/interpreter.dir/Variable.cpp.o"
  "/home/nikita/interpreter/main.cpp" "/home/nikita/interpreter/cmake-build-debug/CMakeFiles/interpreter.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

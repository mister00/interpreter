#include "Lexeme.h"
#include <unordered_map>
#include <sstream>

Lexeme::LexemeType Lexeme::get_type() const{
    return lexeme_type;
}

std::string Lexeme::get_string() const{
    if (lexeme_type != string_const && lexeme_type != identifier){
        throw std::logic_error("illegal cast of lexeme to string");
    }
    return data;
}

double Lexeme::get_double() const{
    if (lexeme_type != double_const){
        throw std::logic_error("illegal cast of lexeme to double");
    }
    double a;
    std::istringstream(data) >> a;
    return a;
}

long Lexeme::get_int() const{
    if (lexeme_type != int_const){
        throw std::logic_error("illegal cast of lexeme to int");
    }
    long a;
    std::istringstream(data) >> a;
    return a;
}

char Lexeme::get_char() const{
    if (lexeme_type != char_const){
        throw std::logic_error("illegal cast of lexeme to char");
    }
    return data[0];
};

Data* Lexeme::get_data() const{
    switch (get_type()) {
        case Lexeme::int_const:
            return new Data_store<int>(get_int());
        case Lexeme::double_const:
            return new Data_store<double>(get_double());
        case Lexeme::char_const:
            return new Data_store<char>(get_char());
        case Lexeme::string_const:
            return new Data_store<std::string>(get_string());
        default:
            throw std::logic_error("can get data only from const");
    }
}

OperatorType Lexeme::get_operator() const {
    const std::unordered_map<LexemeType, OperatorType> commands{
        {add,       c_add},
        {sub,       c_sub},
        {mult,      c_mult},
        {div_float, c_div_float},
        {div_key,   c_div_int},
        {mod_key,   c_mod},
        {and_key,   c_and},
        {or_key,    c_or},
        {not_key,   c_not},
        {assign,    c_assign},
        {greater,   c_greater},
        {g_equal,   c_g_equal},
        {equal,     c_equal},
        {less,      c_less},
        {l_equal,   c_l_equal},
        {not_equal, c_not_equal},
        {in_key,    c_in},
    };
    try {
        return commands.at(lexeme_type);
    } catch (std::out_of_range&){
        throw std::logic_error("not an operator");
    }
}

void Lexeme::form(const LexemeString& string, int line_){
    const std::unordered_map<LexemeString, LexemeType> map = {
        {"+", add},
        {"-", sub},
        {"*", mult},
        {"/", div_float},
        {"(", open_bracket},
        {")", close_bracket},
        {"[", open_sq_bracket},
        {"]", close_sq_bracket},
        {".", dot},
        {",", comma},
        {":", colon},
        {":=", assign},
        {">", greater},
        {">=", g_equal},
        {"=", equal},
        {"<", less},
        {"<=", l_equal},
        {"<>", not_equal},
        {";", semicolon},
        {"^", arrow_up},
        {"and", and_key},
        {"array", array_key},
        {"begin", begin_key},
        {"case", case_key},
        {"const", const_key},
        {"div", div_key},
        {"do", do_key},
        {"downto", downto_key},
        {"else", else_key},
        {"end", end_key},
        {"file", file_key},
        {"for", for_key},
        {"function", function_key},
        {"goto", goto_key},
        {"if", if_key},
        {"c_in", in_key},
        {"label", label_key},
        {"c_mod", mod_key},
        {"nil", nil_key},
        {"not", not_key},
        {"of", of_key},
        {"or", or_key},
        {"packed", packed_key},
        {"procedure", procedure_key},
        {"program", program_key},
        {"record", record_key},
        {"repeat", repeat_key},
        {"set", set_key},
        {"then", then_key},
        {"to", to_key},
        {"type", type_key},
        {"until", until_key},
        {"var", var_key},
        {"while", while_key},
        {"with", with_key},
    };
    line = line_;
    if (map.find(string) != map.cend()){
        lexeme_type = map.at(string);
    } else {
        if (string[0] >= '0' && string[0] <= '9'){
            if (string.find('.') != string.npos || string.find('E') != string.npos){
                lexeme_type = double_const;
                data = string;
            } else {
                lexeme_type = int_const;
                data = string;
            }
        } else if (string[0] == '\''){
            if (string.length() == 3){
                lexeme_type = char_const;
            } else if (string.length() > 3){
                lexeme_type = string_const;
            } else {
                throw std::logic_error("Strange string");
            }
            data = string.substr(1, string.size() - 1);
        } else {
            lexeme_type = identifier;
            data = string;
        }
    }
}

Lexeme::Lexeme(const std::string& string, int line_){
    form(string, line_);
}